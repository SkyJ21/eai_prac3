'''
EAI Prac3 Task 1
Muller Pretorius u21444553
Emile Wepener u21
'''

import numpy as np
import random
import csv
import matplotlib.pyplot as plt

D=12 #num input features
N=1000000 #num data samples
C=3 #num output neurons
H=3 #num neurons in hidden layer


def normalize(val): # Use one-hot encoding for RPS
    if val == 'R':
        return [1,0,0]
    if val == 'P':
        return [0,1,0]
    if val == 'S':
        return [0,0,1]

def reverse_norm(val):
    rounded_val = [round(x) for x in val]
    if rounded_val == [1,0,0]:
        return 'R'
    if rounded_val == [0,1,0]:
        return 'P'
    if rounded_val == [0,0,1]:
        return 'S'

def winning_move(move):# Determine the winning move
    if move == "R":
        return "P"
    elif move == "S":
        return "R"
    else:
        return "S"

data = []
with open('data1.csv', newline='') as csvfile:
    csvreader = csv.reader(csvfile)
    for row in csvreader:
        data.append(row)

new_list=[]
target_values=[]

for row in data:
    for moves in row[0]:
        norm = normalize(moves)
        new_list.extend(norm)
    out = winning_move(row[1])
    norm_out = normalize(out)
    target_values.extend(norm_out)

input_arr= np.array(new_list).reshape(len(data),12) # array containing normalized input
target_values = np.array(target_values).reshape(len(data),3)# array
weights_I_to_h1 =np.random.rand(3, 13)
weights_h1_O = np.random.rand(3, 4)

# print(input_arr)
# print("\n")
# print(target_values)
# print("\n")
# print(weights_I_to_h1)
# print("\n")
# print(weights_h1_O)


def sigmoid(x):
    return 1 / (1 + np.exp(-x))

def sigmoid_deriv(x):
    return sigmoid(x) * (1 - sigmoid(x))

def predict(input_vals, W1, W2):
    b1 =np.array( W1[:,-1:])
    W1= np.array( W1[:,:-1])
    b2= np.array( W2[:,-1:])
    W2= np.array( W2[:,:-1])
    Z_2 = np.dot(input_vals.T, W1.T) +b1.T# 1 is bias value for now
    A_2 = np.array([sigmoid(val) for val in Z_2])
    Z_3 = np.dot(A_2,W2.T) + b2.T# 1 is bias value for now
    Y_hat= np.array([sigmoid(val) for val in Z_3])
    return Y_hat[0]

# print("\n")
# print(reverse_norm(predict(input_arr[0], weights_I_to_h1, weights_h1_O)))

def backpropagation(input_vals, W1,W2,Y, eta,epochs): #only for single data input not multiple yet

    J = []
    b1 =np.array( W1[:,-1:])
    W1= np.array( W1[:,:-1])
    b2= np.array( W2[:,-1:])
    W2= np.array( W2[:,:-1])
    error=0
    for j in range(epochs):
        # forward pass
        Z_2 = np.dot(input_vals[j].T, W1.T) + b1.T  # 1 is bias value for now
        A_2 = np.array([sigmoid(val) for val in Z_2])
        Z_3 = np.dot(A_2, W2.T) + b2.T  # 1 is bias value for now
        Y_hat = np.array([sigmoid(val) for val in Z_3])

        #backpropogation
        delta3 = -(Y[j] - Y_hat) * np.array([sigmoid_deriv(val) for val in Z_3])
        dJdW2 = np.dot(A_2.T, delta3)
        dJdB2 = np.sum(delta3, axis=0, keepdims=True)
        delta2 = np.dot(delta3,W2.T) * np.array([sigmoid_deriv(val) for val in Z_2])
        dJdW1 = np.dot(input_vals[j].reshape(1, -1).T, delta2.reshape(1, -1))
        dJdB1 = np.sum(delta2, axis=0, keepdims=True)

        W1-= eta * dJdW1.T
        W2 -= eta * dJdW2.T
        b1 -= eta * dJdB1.T
        b2 -= eta * dJdB2.T
        error+= sum((1/2)*((Y[j]-Y_hat)**2)[0])
        J.append(error)
        error=0
    W1 = np.hstack((W1,b1))
    W2 = np.hstack((W2,b2))
    return W1, W2,J

epochs=1000000
weights_I_to_h1,weights_h1_O,loss= backpropagation(input_arr,weights_I_to_h1, weights_h1_O,target_values,0.0001,epochs)

print("W1:")
print(weights_I_to_h1)
print("\n")
print("W2:")
print(weights_h1_O)

print("\n")
print(reverse_norm(predict(input_arr[0], weights_I_to_h1, weights_h1_O)))
# print("\n")
# print("One Epoch")
# for i in range(len(input_arr)):
#     print('Predicted values:',predict(input_arr[i], weights_I_to_h1, weights_h1_O))
#     print('Target values:',target_values[i])
# plt.figure(figsize=(8, 6))
# plt.plot(range(1,epochs+1), loss)
# plt.xlabel('Epochs')
# plt.ylabel('Error')
# plt.title('Plot of Error Over Number of Epochs')
# plt.figtext(0.5, 0.02, 'Training rate= '+str(0.0001), ha='center', fontsize=8, wrap=True)
# plt.show()

