'''
EAI PRAC 3 Task 3
Muller Pretorius
Emile Wepener
'''
import numpy as np
import random

W1 =np.array([[-0.58938193, 1.61778546, 0.38816476, 3.27489371, 2.05133157, -3.90232152, 3.2444871, -5.54203501, 4.38603679, 3.10751239, 3.03972155, -5.8169521, 0.42563458],
      [2.68852126, -1.19171161, 0.36292687, 3.23287414, -2.78742156, 1.42080155, -5.33349869, 1.57390822, 5.34162534, 3.36030938, -5.21209118, 3.07250963, 0.3821376],
      [0.18828896, -1.60484134, 3.67866948, -2.36278955, 3.09171412, 0.69821712, 3.39275597, 3.25393593, -5.13927325, -4.67634275, 3.72849677, 3.12021013, 0.66707439]])

W2=np.array( [[-7.92703731, 6.90592893, 6.37872514, -5.64095104],
     [7.45419901, 5.86035391, -6.04556678, -7.0399715],
     [6.97727571, -7.18916872, 5.66250946, -6.16427401]])

def sigmoid(x):
    return 1 / (1 + np.exp(-x))

def sigmoid_deriv(x):
    return sigmoid(x) * (1 - sigmoid(x))

def normalize(val): # Use one-hot encoding for RPS
    if val == 'R':
        return [1,0,0]
    if val == 'P':
        return [0,1,0]
    if val == 'S':
        return [0,0,1]

def reverse_norm(val):
    rounded_val = [round(x) for x in val]
    if rounded_val == [1,0,0]:
        return 'R'
    if rounded_val == [0,1,0]:
        return 'P'
    if rounded_val == [0,0,1]:
        return 'S'
    else:
        return np.random.choice(['R', 'P', 'S'])

def winning_move(move):# Determine the winning move
    if move == "R":
        return "P"
    elif move == "S":
        return "R"
    else:
        return "S"
def predict(input_arr, W1, W2):
    b1 =np.array( W1[:,-1:])
    W1= np.array( W1[:,:-1])
    b2= np.array( W2[:,-1:])
    W2= np.array( W2[:,:-1])
    Z_2 = np.dot(input_arr.T, W1.T) +b1.T# 1 is bias value for now
    A_2 = np.array([sigmoid(val) for val in Z_2])
    Z_3 = np.dot(A_2,W2.T) + b2.T# 1 is bias value for now
    Y_hat= np.array([sigmoid(val) for val in Z_3])
    return Y_hat[0]

def backpropagation(input_arr, W1,W2,Y, eta): #only for single data input not multiple yet
    b1 =np.array( W1[:,-1:])
    W1= np.array( W1[:,:-1])
    b2= np.array( W2[:,-1:])
    W2= np.array( W2[:,:-1])

    # forward pass
    Z_2 = np.dot(input_arr[j].T, W1.T) + b1.T  # 1 is bias value for now
    A_2 = np.array([sigmoid(val) for val in Z_2])
    Z_3 = np.dot(A_2, W2.T) + b2.T  # 1 is bias value for now
    Y_hat = np.array([sigmoid(val) for val in Z_3])

    #backpropogation
    delta3 = -(Y - Y_hat) * np.array([sigmoid_deriv(val) for val in Z_3])
    dJdW2 = np.dot(A_2.T, delta3)
    dJdB2 = np.sum(delta3, axis=0, keepdims=True)
    delta2 = np.dot(delta3,W2.T) * np.array([sigmoid_deriv(val) for val in Z_2])
    dJdW1 = np.dot(input_arr.reshape(1, -1).T, delta2.reshape(1, -1))
    dJdB1 = np.sum(delta2, axis=0, keepdims=True)

    #update weights
    W1-= eta * dJdW1.T
    W2 -= eta * dJdW2.T
    b1 -= eta * dJdB1.T
    b2 -= eta * dJdB2.T

    W1 = np.hstack((W1,b1))
    W2 = np.hstack((W2,b2))

    return W1, W2 ,Y_hat

if input == "":
    n=0
    history = ['R']*4
    input_vals = []
    for val in history:
        input_vals.extend(normalize(val))
    output = reverse_norm(predict(np.array(input_vals), W1, W2))
    prop_rate = 1
else:
    history.pop(0)
    history.append(input)
    input_vals = []
    for val in history:
        input_vals.extend(normalize(val))
    try:
        W1, W2,output = backpropagation(input_vals, W1, W2,normalize(winning_move(input)),0.05)
        #output = reverse_norm(predict(np.array(input_vals), W1, W2))
    except:
        output = np.random.choice(['R', 'P', 'S'])
    history.pop(0)
    history.append(output)
    n+=1